package;

import flash.display.BitmapData;
import flash.display.Shape;
import openfl.display.Sprite;
import openfl.Lib;
import openfl.display.Tilesheet;
import openfl.geom.Rectangle;

/**
 * ...
 * @author gelert
 */
class Main extends Sprite {

	public function new() {
		super();
		var background = new Shape();
		background.graphics.beginFill(0xFF0000);
		background.graphics.drawCircle(100, 100, 50);
		addChild(background);
		var bitmapData = new BitmapData(100, 100, true, 0xFFFFFF00);
		var tilesheet = new Tilesheet(bitmapData);
		tilesheet.addTileRect(new Rectangle(0, 0, 100, 100));
		var container = new Shape();
		addChild(container);
		tilesheet.drawTiles(container.graphics, [100, 100, 0, 0.3], false, Tilesheet.TILE_ALPHA);
		//bitbucket integration with you track tes-6
	}
}